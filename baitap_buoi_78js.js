var numArr = [];
var themSo = function () {
  var number = document.getElementById("txt-number").value * 1;
  numArr.push(number);

  //   hiện thị kết quả
  document.getElementById("result").innerHTML = `
<h5> ${numArr}</h5>

`; //(1)
};

// bài 1

var tsduong = function () {
  var tongSoduong = 0;
  for (var i = 0; i < numArr.length; i++) {
    var num = numArr[i];
    if (num > 0) {
      tongSoduong += num;
    }
  }
  document.getElementById("tsduong").innerHTML = `
<h5> ${tongSoduong}</h5>`;
};
// bài 2

var bnsduong = function () {
  var countsoduong = 0;
  for (var i = 0; i < numArr.length; i++) {
    var num = numArr[i];
    if (num > 0) {
      countsoduong++;
    }
  }
  document.getElementById("bnsduong").innerHTML = `
<h5> ${countsoduong}</h5>`;
};
// bài 3

var sonhonhat = function () {
  var min = numArr[0];
  for (var i = 1; i < numArr.length; i++) {
    var num = numArr[i];
    if (num < min) {
      min = num;
    }
  }
  document.getElementById("sonhonhat").innerHTML = `
<h5> ${min}</h5>`;
};
// bài 4

var sduongnhonhat = function () {
  var cacsoduong = [];

  for (var i = 0; i < numArr.length; i++) {
    var num = numArr[i];
    if (num > 0) {
      cacsoduong += num;
    }
  }
  var minduong = cacsoduong[0];
  for (var i = 1; i < cacsoduong.length; i++) {
    var num = cacsoduong[i];
    if (num < minduong) {
      minduong = num;
    }
  }

  document.getElementById("sduongnhonhat").innerHTML = `
<h5> ${minduong}</h5>`;
};
// bài 5
var schancuoi = function () {
  var chancuoi = -1;
  for (var i = 0; i < numArr.length; i++) {
    var num = numArr[i];
    if (num % 2 == 0) {
      chancuoi = num;
    } else {
      document.getElementById("schancuoi").innerHTML = `
<h5> -1 </h5>`;
    }
  }
  document.getElementById("schancuoi").innerHTML = `
<h5> ${chancuoi}</h5>`;
};
// bài 6
var saukhidoi = function () {
  var vitrit1 = document.getElementById("vitrit1").value * 1;
  var vitrit2 = document.getElementById("vitrit2").value * 1;

  var num1 = numArr[vitrit1];

  // đổi vị trí
  numArr[vitrit1] = numArr[vitrit2];
  numArr[vitrit2] = num1;

  document.getElementById("saukhidoi").innerHTML = `
<h5> ${numArr}</h5>`;
};
// bài 7
var tangdan = function () {
  for (var j = 0; j < numArr.length; j++) {
    var minmang = numArr[j];
    for (var i = j + 1; i < numArr.length; i++) {
      var num = numArr[i];
      if (minmang > num) {
        minmang = num;
        numArr[i] = numArr[j];
        numArr[j] = minmang;
      }
    }
  }

  document.getElementById("tangdan").innerHTML = `
<h5> ${numArr}</h5>`;
};
// bài 8
// kiểm tra số nguyên tố
function isPrimeNumber(p) {
  // so nguyen n < 2 khong phai la so nguyen to
  if (p < 2) {
    return false;
  }
  // check so nguyen to khi n >= 2
  var uocmiendau = Math.sqrt(p);
  for (var b = 2; b <= uocmiendau; b++) {
    if (p % b == 0) {
      return false;
    }
  }
  return true;
}
//  kiểm tra số nguyên tố
var nguyentodau = function () {
  for (var j = 0; j < numArr.length; j++) {
    var num = numArr[j];
    if (isPrimeNumber(num)) {
      break;
    } else {
      document.getElementById("nguyentodau").innerHTML = `
<h5> -1 </h5>`;
    }
  }

  document.getElementById("nguyentodau").innerHTML = `
<h5> ${numArr[j]}</h5>`;
};
// bài 9
var mangsothuc = [];
var themSothuc = function () {
  var number = document.getElementById("mangthuc").value * 1;
  mangsothuc.push(number);

  //   hiện thị kết quả
  document.getElementById("resultmangthuc").innerHTML = `
<h5> ${mangsothuc}</h5>

`;
};
var baonhieusonguyen = function () {
  var count = 0;
  for (var j = 0; j < mangsothuc.length; j++) {
    var num = mangsothuc[j];
    if (Number.isInteger(num)) {
      count++;
    }
    // Number.isInteger kiểm tra số nguyên
  }

  document.getElementById("baonhieusonguyen").innerHTML = `  
<h5> ${count}</h5>`;
};

// bài 10
var sosanhamduong = function () {
  var countam = 0;
  var countduong = 0;
  for (var j = 0; j < numArr.length; j++) {
    var num = numArr[j];
    if (num > 0) {
      countduong++;
    } else if (num < 0) {
      countam++;
    } else {
      countam = countam;
      countduong = countduong;
    }
  }
  if (countam > countduong) {
    document.getElementById("sosanhamduong").innerHTML = `  
<h5>số âm > số dương</h5>`;
  } else if (countam == countduong) {
    document.getElementById("sosanhamduong").innerHTML = `  
<h5>số âm = số dương</h5>`;
  } else {
    document.getElementById("sosanhamduong").innerHTML = `  
<h5> số âm < số dương</h5>`;
  }
};
// end bai 10
/* (1)
<p>Tổng số chẳn : ${tongSoChan}</p>
<p>Số lượng số âm : ${soLuongSoAm}</p>
<p>Tổng số âm : ${tongSoAm}</p>
*/
// numArr
document.getElementById("btn-them-so").addEventListener("click", themSo);
document.getElementById("1").addEventListener("click", tsduong);
document.getElementById("2").addEventListener("click", bnsduong);
document.getElementById("3").addEventListener("click", sonhonhat);
document.getElementById("4").addEventListener("click", sduongnhonhat);
document.getElementById("5").addEventListener("click", schancuoi);
document.getElementById("6").addEventListener("click", saukhidoi);
document.getElementById("7").addEventListener("click", tangdan);
document.getElementById("8").addEventListener("click", nguyentodau);
document
  .getElementById("btn-them-so-thuc")
  .addEventListener("click", themSothuc);
document.getElementById("9").addEventListener("click", baonhieusonguyen);
document.getElementById("10").addEventListener("click", sosanhamduong);
